addpath('/home/chu-chu/Documents/MATLAB/office') % path of office
addpath('/home/chu-chu/Documents/MATLAB/rubic') % path of rubic
addpath('/home/chu-chu/Documents/MATLAB/sphere') % path of sphere

% Read the images in Office and convert it to grayscale
office0_gray = rgb2gray(imread('office.0.bmp'));     office1_gray = rgb2gray(imread('office.1.bmp'));     
office2_gray = rgb2gray( imread('office.2.bmp'));    office3_gray = rgb2gray(imread('office.3.bmp'));     
office4_gray = rgb2gray(imread('office.4.bmp'));     office5_gray = rgb2gray(imread('office.5.bmp'));
office6_gray = rgb2gray(imread('office.6.bmp'));     office7_gray = rgb2gray(imread('office.7.bmp'));     
office8_gray = rgb2gray(imread('office.8.bmp'));     office9_gray = rgb2gray(imread('office.9.bmp'));     
office10_gray = rgb2gray(imread('office.10.bmp'));   office11_gray = rgb2gray(imread('office.11.bmp'));
office12_gray = rgb2gray(imread('office.12.bmp'));   office13_gray = rgb2gray(imread('office.13.bmp'));    
office14_gray = rgb2gray(imread('office.14.bmp'));   office15_gray = rgb2gray(imread('office.15.bmp'));   
office16_gray = rgb2gray(imread('office.16.bmp'));   office17_gray = rgb2gray(imread('office.17.bmp'));
office18_gray = rgb2gray(imread('office.18.bmp'));   office19_gray = rgb2gray(imread('office.19.bmp'));

%store gray images in a list
gray_office = {office0_gray, office1_gray, office2_gray, office3_gray, office4_gray, office5_gray, office6_gray,...
    office7_gray, office8_gray, office9_gray, office10_gray, office11_gray, office12_gray, office13_gray, office14_gray,...
    office15_gray, office16_gray, office17_gray, office18_gray, office19_gray};

%Read the images in Sphere and convert to grayscale
sphere0_gray = rgb2gray(imread('sphere.0.bmp'));     sphere1_gray = rgb2gray(imread('sphere.1.bmp'));     
sphere2_gray = rgb2gray( imread('sphere.2.bmp'));    sphere3_gray = rgb2gray(imread('sphere.3.bmp'));     
sphere4_gray = rgb2gray(imread('sphere.4.bmp'));     sphere5_gray = rgb2gray(imread('sphere.5.bmp'));
sphere6_gray = rgb2gray(imread('sphere.6.bmp'));     sphere7_gray = rgb2gray(imread('sphere.7.bmp'));     
sphere8_gray = rgb2gray(imread('sphere.8.bmp'));     sphere9_gray = rgb2gray(imread('sphere.9.bmp'));     
sphere10_gray = rgb2gray(imread('sphere.10.bmp'));   sphere11_gray = rgb2gray(imread('sphere.11.bmp'));
sphere12_gray = rgb2gray(imread('sphere.12.bmp'));   sphere13_gray = rgb2gray(imread('sphere.13.bmp'));    
sphere14_gray = rgb2gray(imread('sphere.14.bmp'));   sphere15_gray = rgb2gray(imread('sphere.15.bmp'));   
sphere16_gray = rgb2gray(imread('sphere.16.bmp'));   sphere17_gray = rgb2gray(imread('sphere.17.bmp'));
sphere18_gray = rgb2gray(imread('sphere.18.bmp'));   sphere19_gray = rgb2gray(imread('sphere.19.bmp'));

%store gray images in a list
gray_sphere = {sphere0_gray, sphere1_gray, sphere2_gray, sphere3_gray, sphere4_gray, sphere5_gray, sphere6_gray,...
    sphere7_gray, sphere8_gray, sphere9_gray, sphere10_gray, sphere11_gray, sphere12_gray, sphere13_gray, sphere14_gray,...
    sphere15_gray, sphere16_gray, sphere17_gray, sphere18_gray, sphere19_gray};

%Read images from rubic, they're already in grayscale so we don't have to
%convert them
rubic0_gray = imread('rubic.0.bmp');      rubic1_gray = imread('rubic.1.bmp');      rubic2_gray = imread('rubic.-1.bmp');
rubic3_gray = imread('rubic.2.bmp');      rubic4_gray = imread('rubic.3.bmp');      rubic5_gray = imread('rubic.4.bmp');
rubic6_gray = imread('rubic.5.bmp');      rubic7_gray = imread('rubic.6.bmp');      rubic8_gray = imread('rubic.7.bmp');     
rubic9_gray = imread('rubic.8.bmp');      rubic10_gray = imread('rubic.9.bmp');     rubic11_gray = imread('rubic.10.bmp');    
rubic12_gray = imread('rubic.11.bmp');    rubic13_gray = imread('rubic.12.bmp');    rubic14_gray = imread('rubic.13.bmp');    
rubic15_gray = imread('rubic.14.bmp');    rubic16_gray = imread('rubic.15.bmp');    rubic17_gray = imread('rubic.16.bmp');    
rubic18_gray = imread('rubic.17.bmp');    rubic19_gray = imread('rubic.18.bmp');    rubic20_gray = imread('rubic.19.bmp');

% store gray images in a list
gray_rubic = {rubic0_gray, rubic1_gray, rubic2_gray, rubic3_gray, rubic4_gray,...
    rubic5_gray, rubic6_gray, rubic7_gray, rubic8_gray, rubic9_gray,...
    rubic10_gray, rubic11_gray, rubic12_gray, rubic13_gray, rubic14_gray,...
    rubic15_gray, rubic16_gray, rubic17_gray, rubic18_gray, rubic19_gray};

% for i=1:5;
%     current = gray_rubic{i};
%     next = gray_rubic{i+1};
%     [u,v] = HornSchunk(current,next,5,10);
%     PlotFlow(u, v, current, 5, 5);
% end

for i=1:5
    current = gray_rubic{i};
    next = gray_rubic{i+1};
    display = gray_rubic{i+1};
    [Vx, Vy] = HornSchunk(current,next, 300, 9000);
    [cx, cy, vx, vy] = Arrows(u, v, 20, gray_rubic{i+1});
    figure();
    imshow(display);
    hold on;
    % draw the velocity vectors
    quiver(cx, cy, vx, vy, 'r', 'LineWidth',1.3)
end