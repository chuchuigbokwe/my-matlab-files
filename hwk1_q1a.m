v = -2:0.2:2;
[x11,x21] = meshgrid(v);
fx = x11^2 + x11*x21 + x11 + (3*x21^2)/2 + x21;
[px1,px2] = gradient(fx,-2,2);
for i 