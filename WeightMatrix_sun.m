% Take half window size and weight type as Input and Weight Matrix as Output
function [Weight] = WeightMatrix(halfWindow, weighttype, sigma)
% Generate window weight. 
% Weighttype 0 Uniform Window
% Weighttype 1 Gaussian Window with mean 0 variance 0.5
weightsize = 2*halfWindow +1; % Size of the Weight Matrix
if weighttype == 0 % Unifrm Window
    weight = (1/weightsize)^2 * (ones(weightsize)); % Normalize the Matrix
elseif weighttype == 1 % Gaussian Window
    weight = fspecial('gaussian',weightsize, sigma); % Gaussian Filter
end

% Transfer from weight to Weight Matrix
weight = weight'; % Matrix Transpose
weight = weight(:); % Transfer in to column vector
Weight = diag(weight); % Diagnoal Weight Matrix
% As Verification, the sum(sum(Weight)) should be 1
end
