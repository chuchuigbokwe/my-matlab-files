function l = get_line_by_two_points(x,y)
x1 = [x(1),y(1),1]';
x2 = [x(2),y(2),1]';
l = cross(x1,x2)

% Normalize the line so a and b have norm 1
l = l/sqrt(l(1)^2 + l(2)^2)