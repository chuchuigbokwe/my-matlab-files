x = linspace(-1,1,50);
y = x;
a = 3;
c = 0.5;
[xx,yy] = meshgrid(x,y);
z = c*sin(2*pi*a*sqrt(xx.^2 + yy.^2));
surf(xx,yy,z), colorbar, xlabel('x'), ylabel('y'), zlabel('z'),...
title('f(x,y) = csin(2\pia\surd(x^2 + y^2))');
figure;
mesh(xx,yy,z), colorbar, xlabel('x'),ylabel('y'), zlabel('z'),...
title('f(x,y)=csin(2\pia\surd(x^2 + y^2))');
