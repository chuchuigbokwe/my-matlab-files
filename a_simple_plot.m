x = 0:0.1:20;
y = exp(-x/10).*sin(x);
plot(x,y),grid on, xlabel('x'),ylabel('f(x) = e^{-x/10}sin(x)'), title('A simple plot')