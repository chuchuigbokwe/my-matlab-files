n=0; %initialize iteration counter 
eps=1;          %initialize error 
x=ones(10,1);%[1;1];        %set starting value

%Computation loop 
while eps>1e-10 & n<100 
    gradf= (2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1);     %[4*(x(1)-x(2))^3+4*x(1)-1;-4*(x(1)-x(2))^3+2*x(2)+2];  %gradf(x) 
    eps= norm(gradf);           %abs(gradf(1))+abs(gradf(2));                             %error 
    Hf=  (2*exp(transpose(x)*x)*(exp(transpose(x)*x) + 2*transpose(x)*x + 1))/(exp(transpose(x)*x) + 1)^2;  %[12*(x(1)-x(2))^2+4,-12*(x(1)-x(2))^2;...                 %Hessean 
                                                                 %-12*(x(1)-x(2))^2,12*(x(1)-x(2))^2+2]; 
    y=x-Hf\gradf;                                                %iterate 
    x=y;                                                         %update x 
    n=n+1;                                                       %counter+1 
end 
n,x,eps,        %display end values

