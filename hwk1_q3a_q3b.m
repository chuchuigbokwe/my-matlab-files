% Create a 10 x 1 symbolic matrix
x = sym('x%d%d', [10 1])
fx = log(1 + exp(transpose(x)*x))

% Q3a
% first order condition for all stationary points
% differentiate fx, equate to zero and solve for x
dfx = diff(fx)
solve(dfx ==0, x)

% Q3b
%
ddfx = diff(fx,2)
solve(ddfx == 0, x)
