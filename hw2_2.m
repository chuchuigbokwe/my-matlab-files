load('hw_2_sin_data.mat');

[x0,x1] = meshgrid(-3:0.1:3);
%[x0] = linspace(-3,3,75);
%[x1] = linspace(-3,3,75);
a = data(:,1);
b = data(:,2);
f = zeros(size(x0));

for i = 1:length(x0)
    for j = 1:length(x1)
        for k = 1:length(a)
            f(i,j) = (x0(i,i)*sin(2*pi.*a(k)*x1(j,j))-b(k)).^2;
%    f(i) = (x0(i).*sin(2*pi.*z*x1(i))-z).^2;
        end
    end
end

surf(x0,x1,f), xlabel('x1'),ylabel('x0'),zlabel('cost function')