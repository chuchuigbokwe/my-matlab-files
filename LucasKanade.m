% Take two frame of images as Input as well as the window size and weight type return Vx and Vy
function [Vx, Vy] = LucasKanade(image1, image2, WindowSize, weighttype, sigma)
% sigma if for Gaussian Weight Matrix Generator, if weight type is 0
% please set sigma to be 0

Vx = zeros(size(image1)); % set blanket Vx
Vy = zeros(size(image2)); % set blanket Vy
halfWindow = round(WindowSize/2);  % Set half the window size for calculation
[Ix, Iy, It] = IxIyIt(image1, image2); % Calculate Ix Iy It

% Generate window weight. 
% Weighttype 0 Uniform Window
% Weighttype 1 Gaussian Window with mean 0 variance 0.5
[Weight] = WeightMatrix(halfWindow, weighttype, sigma);

% Use half window size in consideration of boundary, make sure no
% calculation outside boundary
for i = halfWindow+1 : size(Ix,1)-halfWindow
   for j = halfWindow+1 : size(Ix,2)-halfWindow
      % Extract the Ix Iy It value in the window
      tempIx = Ix(i-halfWindow : i+halfWindow, j-halfWindow : j+halfWindow);
      tempIy = Iy(i-halfWindow : i+halfWindow, j-halfWindow : j+halfWindow);
      tempIt = It(i-halfWindow : i+halfWindow, j-halfWindow : j+halfWindow);
      % temp Ix Iy It matrix is labeld with number [1,2,3;4,5,6;7,8,9] N x X
      % We have to transfer it into [1,2,3,4,5,6,7,8,9]' N^2 x 1
      % Matrix Transpose
      tempIx = tempIx';
      tempIy = tempIy';
      tempIt = tempIt';
      % Make the matrix in a column vector
      AIx = tempIx(:); % Ix
      AIy = tempIy(:); % Iy
      % concatenate and result in A and b
      b = -tempIt(:); % -It
      A = [AIx AIy]; % A matrix
      % Calculate V vector with Weight
      V = pinv(A'*Weight*Weight*A)*A'*Weight*Weight*b;
      % Extract the Vx and Vy from V
      Vx(i,j)=V(1);
      Vy(i,j)=V(2);
   end;
end;
end