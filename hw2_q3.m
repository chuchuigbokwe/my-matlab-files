load('ohm.mat')
current_inv = 1./current
D = [1,length_of_wire(1);1,length_of_wire(2);1,length_of_wire(3);1,length_of_wire(4);1,length_of_wire(5)];
b = [current_inv(1);current_inv(2);current_inv(3);current_inv(4);current_inv(5)];
% Solving the Least Squares
x = pinv(D)*b;
% The new equation of the line
y = x(1) + x(2)*(length_of_wire);
% plot(length_of_wire,current_inv,':')
plot(length_of_wire,current_inv,'-bs',length_of_wire,y,'r','MarkerEdgeColor','k','MarkerFaceColor','k'),xlabel('length of wire'),ylabel('1/current')...
    ,title('Graph of Inverse of Current vs Length of Wire')
t = 0:5:100;
figure
plot(length_of_wire,current,'rs')
hold on
y2 = (155.9)./(t + 32.9);
plot(t,y2),xlabel('length of wire'),ylabel('current'), title('original feature spaces fit to line')