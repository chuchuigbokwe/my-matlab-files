% 2014/12/20
% by Y Jay

function [xopt,fopt,niter,gnorm,dx] = newton_opt(varargin)

if nargin==0
    % define starting point
    x0 = [-1.5 2.9]';
elseif nargin==1
    % if a single input argument is provided, it is a user-defined starting
    % point.
    x0 = varargin{1};
else
    error('Incorrect number of input arguments.')
end

% termination tolerance
tol = 1e-5;

% maximum number of allowed iterations
maxiter = 100;

% minimum allowed perturbation
dxmin = 1e-5;

% initialize gradient norm, optimization vector, iteration counter, perturbation
gnorm = inf; x = x0; niter = 0; dx = inf;



% gradient descent algorithm:
while and(gnorm>=tol, and(niter <= maxiter, dx >= dxmin))
    % calculate gradient:
    g = grad(x);
    gnorm = norm(g);
    % take step: by Hesse and grad
    hes= hesse(x);
    %[lamda,~] = fminbnd(@(lamda) iter(lamda,hes,x,g),0,10)
    
    xnew = x - hes\g;
    x = xnew;
    
end
end

% define the gradient of the objective
function g = grad(x)
g = (2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1);
end

function hes = hesse(x)
hes = (2*exp(transpose(x)*x)*(exp(transpose(x)*x) + 2*transpose(x)*x + 1))/(exp(transpose(x)*x) + 1)^2;
end

function frosen = iter(lamda,hes,A,B)
    xnew = A - lamda*hes\B;
    frosen = Rosenbrock(xnew(1),xnew(2));
end