function L2reg_cross_validation_regression()
% Here we illustrate 3-fold cross validation for regression

% Variables
deg = 10;
lams = logspace(-5,0,30);

% load points and plot
[a,b] = load_data();

% split points into 3 equal sized sets and plot
c = split_data(b);

% do 3-fold cross-validation
cross_validate(a,b,c,lams,deg);  

function c = split_data(b)
    % split data into 3 equal sized sets
    K = length(b);
    order = randperm(K);
    c = ones(K,1);
    K = round((1/3)*K);
    c(order(K+1:2*K)) =2;
    c(order(2*K+1:end)) = 3;

    % plot train/test sets for each cross-validation instance
    for j = 1:3
        a_r = a(find(c == j));
        b_r = b(find(c == j));
        a_t = a(find(c~=j));
        b_t = b(find(c~=j));
        subplot(2,3,j)
        box on
        plot_pts(a_r,b_r,a_t,b_t);
    end
end
        
function cross_validate(a,b,c,lams,deg)  
    %%% performs 3-fold cross validation

    % solve for weights and collect test errors
    % build features
    A = [];
    for k = 0:deg
        A = [A  a.^k];
    end
    U = zeros(size(A,2));
    U(2:end,2:end) = eye(size(A,2)-1);
    test_errors = [];
    train_errors = [];
    for i = 1:length(lams)
        % generate features
        train_resids = [];
        test_resids = [];
        for j = 1:3
            A_1 = A(find(c ~= j),:);
            b_1 = b(find(c ~= j));
            A_2 = A(find(c==j),:);
            b_2 = b(find(c==j));
            x = linsolve(A_1'*A_1 + lams(i)*U,A_1'*b_1);
            
            resid = norm(A_2*x - b_2)/numel(b_2);
            test_resids = [test_resids resid];
            resid = norm(A_1*x - b_1)/numel(b_1);
            train_resids = [train_resids resid];
        end
        test_errors = [test_errors; test_resids];
        train_errors = [train_errors; train_resids];
    end

    % find best parameter per data-split
    for i = 1:3
        
        %%% find the best performer (per split) and plot it %%%
        [val,j] = min(test_errors(:,i));
        A_1 = A(find(c ~= i),:);
        b_1 = b(find(c ~= i));
        x = linsolve(A_1'*A_1 + lams(j)*U,A_1'*b_1);
        
        % output model
        subplot(2,3,i) 
        plot_poly(x,'b',deg)

        %%% find the worst performer (per split) and plot it %%%
        [val,j] = max(test_errors(:,i));
        A_1 = A(find(c ~= i),:);
        b_1 = b(find(c ~= i));
        x = linsolve(A_1'*A_1 + lams(j)*U,A_1'*b_1);
        
        % output model
        subplot(2,3,i)
        plot_poly(x,'r',deg)

        %axis square 
        box on
        xlabel('a','Fontsize',14,'FontName','cmmi9')
        ylabel('b','Fontsize',14,'FontName','cmmi9')
        set(get(gca,'YLabel'),'Rotation',0)
        set(gca,'YTickLabel',[])
        set(gca,'YTick',[])
        set(gcf,'color','w');
        axis([0 1 -2 2])
    end
    test_ave = mean(test_errors');
    [val,j] = min(test_ave);
    % build features
    xmin = linsolve(A'*A + lams(j)*U,A'*b);
    
    % plot poly
    subplot(2,3,5)
    plot_poly(xmin,'m',deg)
    
  % clean up plot
    axis([0 1 -2 2])
    
    %axis square 
    box on
    xlabel('a','Fontsize',14,'FontName','cmmi9')
    ylabel('b','Fontsize',14,'FontName','cmmi9')
    set(get(gca,'YLabel'),'Rotation',0)
    set(gca,'YTickLabel',[])
    set(gca,'YTick',[])
    set(gcf,'color','w');
    set(gca,'FontSize',12); 

    % plot training and testing errors
    figure(2)
    s = mean(test_errors');
    semilogx(lams,s,'--','MarkerEdgeColor',[1 0.7 0],'MarkerFaceColor',[1 0.7 0])
    hold on
    semilogx(lams,s,'o','MarkerEdgeColor',[1 0.7 0],'MarkerFaceColor',[1 0.7 0])
    hold on
    
    s = mean(train_errors');
    semilogx(lams,s,'--','MarkerEdgeColor',[0.1 0.8 1],'MarkerFaceColor',[0.1 0.8 1])
    hold on
    semilogx(lams,s,'o','MarkerEdgeColor',[0.1 0.8 1],'MarkerFaceColor',[0.1 0.8 1])
    set(gcf,'color','w');
    box on
    xlabel('lambda','Fontsize',14,'FontName','cmr10')
    ylabel('error','Fontsize',14,'FontName','cmr10')
    set(get(gca,'YLabel'),'Rotation',90)
    set(gca,'YTickLabel',[])
    set(gca,'YTick',[])
    set(gcf,'color','w');
    set(gca,'FontSize',12); 

    figure(3)
    bar(0:length(xmin)-1,xmin);
    xlabel('x-index','Fontsize',14,'FontName','cmr10')
    set(gcf,'color','w');
    box off
end
    
function plot_poly(x,color,deg)
    s = [0:0.001:1];
    t = 0;
    for k = 1:deg+1;
        t = t + x(k)*s.^(k-1);
    end
    plot(s,t,color,'LineWidth',1.25)
end

function plot_pts(a_r,b_r,a_e,b_e)
    % plot train
    hold on
    plot(a_e,b_e,'o','MarkerEdgeColor',[0.1 0.8 1],'MarkerFaceColor',[0.1 0.8 1])
    % plot test
    hold on
    plot(a_r,b_r,'o','MarkerEdgeColor',[1 0.7 0],'MarkerFaceColor',[1 0.7 0])

end

function [a,b] = load_data()
        
    % load points and plot
    data = load('discrete_sin_data.mat');
    data = data.c;
   
    a = data(:,1);
    b = data(:,2);

    % plot
    subplot(2,3,5)
    plot(a,b,'o','MarkerEdgeColor','k','MarkerFaceColor','k')
    hold on
    xlabel('a','Fontsize',14,'FontName','cmr10')
    ylabel('b','Fontsize',14,'FontName','cmr10')

end

end



