%This file processes all the *.mat files in a given folder. The variables
%dirName and 'volserror may be changed to suit the .mat type. This find the
%max and min of the zero degree row of  noise-added electrosense data from 
%compsole. The trials are processed in batch then the amplitudes and aspect
%ratios are saved to an output .mat file with the aspect ratio in the name
%of the file.

dirName = 'C:\Users\Kathleen\Documents\MATLAB\Final_Project\noisy_data';  %# folder path
files = dir( fullfile(dirName,'*.mat') ) ;  %# list all *.mat files
files = {files.name}';                      %'# file names

data = cell(numel(files),1);                %# store file contents
for k=1:numel(files)
    fname = fullfile(dirName,files{k});     %# full path to file
    clear data;
    data=load(fname);
      hipeak=max(data.volserror(1,:));      %this can b adjusted according 
      lopeak=min(data.volserror(1,:));      %the format of the .mat file
      amp=(hipeak-lopeak)/2;
      feat(k)=amp;
    end

AR=[1 2 4];
ar1 = feat(1:5)';
ar2 = feat(6:10)';
ar3 = feat(11:15)';
AMPMAT = horzcat(ar1,ar2,ar3)

save('ar1_ar2_ar4-amps.mat','AR', 'AMPMAT'); %output file