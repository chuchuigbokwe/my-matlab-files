% Take two frame of images as Input as well as the iteration depth and lambda parameter return Vx and Vy
function [Vx, Vy] = HornSchuncke(image1, image2, iteration, lambda)
% iteration is number of iterations
% lambda is a parameter that reflects the influence of the smoothness term.

% Smooth and set data type the image before processing
image1=double(image1);
image2=double(image2);
image1=Smoothing(image1,1);
image2=Smoothing(image2,1);

Vx = zeros(size(image1));% set blanket Vx
Vy = zeros(size(image2));% set blanket Vy
[Ix, Iy, It] = IxIyIt(image1, image2); % Calculate the derivaive

% Weighting Average Kernal
Wkernel = [1/12, 1/6, 1/12; 1/6, 0, 1/6; 1/12, 1/6, 1/12];

% Iterations
for i=1:iteration
    % Compute local averages of the flow vectors
    VxAvg = conv2(Vx,Wkernel,'same');
    VyAvg = conv2(Vy,Wkernel,'same');
    % Compute flow vectors constrained by its local average and the optical flow constraints
    Vx = VxAvg - ( Ix .* ( ( Ix .* VxAvg ) + ( Iy .* VyAvg ) + It ) ) ./ ( lambda + Ix.^2 + Iy.^2) ;
    Vy = VyAvg - ( Iy .* ( ( Ix .* VxAvg ) + ( Iy .* VyAvg ) + It ) ) ./ ( lambda + Ix.^2 + Iy.^2);
end
end