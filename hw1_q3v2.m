function hw1_q3()
% convex_hess_surrogate.m is a toy wrapper to illustrate the path
% taken by Hessian descent (or Newton's method).  The steps are evaluated 
% at the objective, and then plotted.  For the first 5 iterations the
% quadratic surrogate used to transition from point to point is also plotted.
% The plotted points on the objective turn from green to red as the 
% algorithm converges (or reaches a maximum iteration count, preset to 50).
% The (convex) function here is
%
% f(x) = log(1 + exp(x^2))

%%%Use the first order condition to find all stationary points of the
%%%function
%syms y;
%f = log(1+exp(y.'*y));
%g = diff(f,y);
%d = solve(g);
%clear all;

%%%Use the second order definition of convexity to verify that f is convex



%%% create function, choose initial point, to perform hessian descent on %%%
%range = 1.1;    % symmetric range over which to plot the function
%[a,b] = make_fun(range);
%x0 = choose_starter(a,b,range);
x0 = ones(10,1);
%disp(['You picked starting point x0 = ',num2str(x0)])

%%% perform hessian descent %%%
[x,in,out] = hessian_descent(x0);

%%% plot function with hessian descent objective evaluations %%%
%plot_it_all(in,out,range)

% performs hessian descent
function [x,in,out] = hessian_descent(x)

    % initializations
    grad_stop = 10^-3;
    max_its = 50;
    iter = 1;
    grad_eval = 1;
    in = [x(1)];
    out = [obj(x)];
    
    % main loop
    while norm(grad_eval) > grad_stop && iter <= max_its
        % take gradient step
        grad_eval = grad(x)
        hess_eval = hess(x)
        %x = x - grad_eval/hess_eval;
        x = x - pinv(hess_eval)*grad_eval;
        
        % update containers
        in = [in; x(1)];
        out = [out; log(1+exp((x.')*x))];

        % update stopers
        iter = iter + 1;
    end
    plot(out)
    title('Objective Value at Each Iteration of Newtons Method')
    xlabel('Iterations')
    ylabel('Objective Value')
    
end

% evaluate the objective
function z = obj(y)
%    z = log(1 + exp(y^2));
    z = log(1 + exp((y.')*y));
end 

% evaluate the gradient
function z = grad(y)
    z = (2*exp((y.')*y)*y)/(1+exp((y.')*y));
end 

% evaluate the hessian
function z = hess(y)
    %z = (4*exp((y.')*y))*((y.')*y)/(exp((y.')*y) + 1)^2;
    z = (2*exp((y.')*y)*(2*((y.')*y) + exp((y.')*y) + 1))/(exp((y.')*y) + 1)^2;
end 
       
% evaluate surrogate
function z = surrogate(y,x)
    z = obj(y) + grad(y)*(x - y) + 1/2*hess(y)*(x - y).^2;
end

end