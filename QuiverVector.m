% Take Vx Vy interval and opject image and return vector for quiver plot
function [qx, qy, vx, vy] = arrows(u, v, interval, image2)
vx = u(1:interval:end, 1:interval:end);
vy = v(1:interval:end, 1:interval:end);
% get coordinate for vx and vy in the original frame
[m, n] = size(image2);
[x,y] = meshgrid(1:n, 1:m);
qx = x(1:interval:end, 1:interval:end);
qy = y(1:interval:end, 1:interval:end);
end