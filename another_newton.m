x = [1; 1];%-2;
Tol = 10^-3;
count = 0;
dx=1;   %this is a fake value so that the while loop will execute
grad=(2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1); %-13;    % because f(-2)=-13
fprintf('step      x           dx           f(x)\n')
fprintf('----  -----------  ---------    ----------\n')
fprintf('%3i %12.8f %12.8f %12.8f\n',count,x,dx,grad)
xVec=x;fVec=grad;
while (dx > Tol || norm(grad)>Tol)  %note that dx and f need to be defined for this statement to proceed
    count = count + 1;
    hess = (2*exp(transpose(x)*x)*(exp(transpose(x)*x) + 2*transpose(x)*x + 1))/(exp(transpose(x)*x) + 1)^2; %3*x^2 + 3;
    xnew = x - (grad/hess);   % compute the new value of x
    dx=abs(x-xnew);          % compute how much x has changed since last step
    x = xnew;
    grad = (2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1); %x^3 + 3*x + 1;       % compute the new value of f(x)
    fprintf('%3i %12.8f %12.8f %12.8f\n',count,x,dx,grad)
end

% This produces the following output: