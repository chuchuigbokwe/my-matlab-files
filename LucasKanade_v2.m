
function[u,v] = LucasKanade_v2(im1, im2, windowSize,hsize,sigma);
[Ix,Iy,It] = ImageDerivatives(im1,im2);
u = zeros(size(im1));
v = zeros(size(im2));
hw = round(windowSize/2);
[Weight] = WeightMatrix(hw, hsize, sigma);
for i = hw+1:size(Ix,1)-hw
    for j = hw+1:size(Ix,2)-hw
        Ix_new = Ix(i-hw:i+hw, j-hw:j+hw);
        Iy_new = Iy(i-hw:i+hw, j-hw:j+hw);
        It_new = It(i-hw:i+hw, j-hw:j+hw);
        
        Ix_new = Ix_new.';
        Iy_new = Iy_new.';
        It_new = It_new.';
        
        Ix_new = Ix_new(:);
        Iy_new = Iy_new(:);
        b = It_new(:);
        
        A= [Ix_new, Iy_new];
        
        d = pinv(A'*Weight*Weight*A)*A'*Weight*Weight*b;
        
        u(i,j) = d(1);
        v(i,j) = d(2);
    end;
end;

