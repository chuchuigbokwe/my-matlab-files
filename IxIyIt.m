% Compute the partial derivative Ix Iy of the image1 and It from image 1 to image 2
function [Ix, Iy, It] = IxIyIt(image1, image2)
% Ix Iy It are in form of Matrix here, not column vector
% Calculation must be in double for new version Matlab
Ix = conv2(double(image1), double([-1 1; -1 1]), 'same'); % partial devitive with respect of x
Iy = conv2(double(image1), double([-1 -1; 1 1]), 'same'); % partial devitive with respect of y
It = conv2(double(image1), 1.0*ones(2), 'same') + conv2(double(image2), -1.0*ones(2), 'same'); % partial devitive with respect of t
end