x = [-10:0.5:10];
% theta = linspace(0,10,21);
y = 1./x;
y1 = sin(x) .* cos(x);
y2 = 2*x.^2 - 3*x +1;
subplot(2,2,1), plot(x,y,'b--'),grid on,title('A graph of y=1/x with a blue dashed line'), xlabel('x'),ylabel('y=1/x');
subplot(2,2,2), plot(x,y1,'r:'),grid on,title('A graph of y=cos(x)sin(x) with a red dotted line'), xlabel('x'),ylabel('y=sin(x)cos(x)');
subplot(2,2,3), plot(x,y2,'r +'),grid on,title('A graph of y = 2*x^2 - 3*x +1 with a red cross markers'), xlabel('x'),ylabel('y = 2*x^2 - 3*x +1');