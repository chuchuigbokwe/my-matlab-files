phi= linspace(-pi/4,pi/4,50);
x = cos(phi) .* sqrt(2*cos(2 * phi));
y = sin(phi) .* sqrt(2*cos(2 * phi));
plot(x,y,'r'),grid on, xlabel('x'),ylabel('y'),title('Plot of a leminscate'),axis auto;