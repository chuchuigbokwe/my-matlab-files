% ex5_q1
% Calculate the pressure at the bottom of a vented liquid storage tank

% Chu-Chu Igbokwe, 11/01/2015

% Variable Dictionary
% p_abs_bottom = absolut pressure at the bottom of the storage tank
% rho = liquid density (kg/m^3)
% g = acceleration due to gravity (m/s^2)
% h = height of the liquid (m)
% p_outside = outside atmospheric pressure

clear all;
clc;

rho = 1000;
g = 9.81;
h = 6.4008;
p_outside = 1.013E5; 
p_abs_bottom = rho * g * h + p_outside