c = 0.5;
u = linspace(-pi,pi,100);
v = linspace(0,pi,100);
[u,v] = meshgrid(u,v);
x = (c + cos(v).* cos(u));
y = (c + cos(v).* sin(u));
z = sin(v).*cos(v);
surf(x,y,z),colorbar,xlabel('x'),ylabel('y'),zlabel('z'),...
title('Elliptic torus'), shading interp;