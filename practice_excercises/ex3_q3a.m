r = 5;
theta = linspace(0,2*pi,100);
x = r*cos(theta);
y = r*sin(theta);
plot(x,y,'b'),grid on,xlabel('x'),ylabel('y'),title ('Plot for a circle'),axis equal;