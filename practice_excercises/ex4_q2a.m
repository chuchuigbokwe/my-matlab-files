u = linspace(0,2*pi,50);
v = u;
[u,v] = meshgrid(u,v);
x = sin(u);
y = sin(v);
z = sin(u + v);
surf(x,y,z),colorbar,xlabel('x'),ylabel('y'),zlabel('z'),...
title('Sine Surface'), shading interp;