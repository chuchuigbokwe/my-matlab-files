t = linspace(0,2*pi,1000);
a = 10;
b = 1;
c = 0.3;
x = cos(t).* sqrt(b^2 - c^2 * (cos(a.*t)).^2);
y = sin(t).* sqrt(b^2 - c^2 * (cos(a.*t)).^2);
z = c * cos(a.*t);
plot3(x,y,z,'g'), grid on, xlabel('x'),ylabel('y'),zlabel('z'),...
title('Sine Surface');