t = linspace(0,10*pi,100);
c = 5;
x = sin(t./(2*c)).* cos(t);
y = sin(t./(2*c)).* sin(t);
z = cos(t./(2*c));
plot3(x,y,z,'b'),grid on;xlabel('x'),ylabel('y'),title('Spherical Helix');
