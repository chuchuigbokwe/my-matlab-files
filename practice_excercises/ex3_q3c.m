phi = linspace(0,6*pi,150);
k = 0.1;
x = exp(k .* phi) .* cos(phi);
y = exp(k .* phi) .* sin(phi);
plot(x,y,'r'),grid on, xlabel('x'),ylabel('y'),title('Plot of a Logarithmic Spiral'),axis equal;