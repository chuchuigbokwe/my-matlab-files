t = linspace(0,10,250);
v0 = 230;
f = 50;
R = 500;
L = 650E-3;
w = 2*pi*f;
Z = (R + w*L*j);
phi = atan(w*L/R);
I = (v0/abs(Z))* (cos(w.*t - phi) - exp(-t.*R/L) .* cos(phi));
plot (t,I);