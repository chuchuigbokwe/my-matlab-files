a = 1;
b = 1.5;
c = 0.3;
phi = [0:1:360];
s = a* cosd(phi) + sqrt(b^2 - (a*sind(phi) - c).^2);
plot(phi,s),title('A graph of s = acos(phi) + sqrt(b^2 - (asin(phi) - c)^2)'),xlabel('phi'),ylabel('s = acos(phi) + sqrt(b^2 - (asin(phi) - c)^2)');