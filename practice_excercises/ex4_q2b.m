r1 = 0.5;
r2 = r1;
t = 1.5;
u = linspace(0,10*pi,50);
v = u;
[u,v] = meshgrid(u,v);
x = (1 - r1 * cos(v)).* cos(u);
y = (1 - r1 * cos(v)).* sin(u);
z = r2 * (sin(v) + (t.*u)/pi);
surf(x,y,z),colorbar,xlabel('x'),ylabel('y'),zlabel('z'),...
title('Spring '), shading interp;
