
function [temp_table] = Celsius_to_Fahrenheit (T_lower,T_upper);
% ex6_q1_Celsius_to_Fahrenheit is a function that takes a lower and upper
% temperature value in Celsius and calculates a table of corresponding
% Fahrenheit values

% Chu-Chu Igbokwe 11/01/2015

%Variable Dicitonary
% T_lower       input   lower temperature (C)
% T_upper       input   upper temperature (C)   
% temp_table    output  matrix containing both Celsius and Fahrenheit
% values
% Celsius       local   vector containing Celsius values
% Fahrenheit    local   vector containing Fahrenheit values

T_lower = input('Enter the lower range of the temperature in Celsius: ');
T_upper = input('Enter the upper range of the temperature in Celsius: ');

Celsius = [T_lower:T_upper]';
Fahrenheit = 9/5 * Celsius + 32;
temp_table = [Celsius Fahrenheit]