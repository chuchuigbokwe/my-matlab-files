%% Preface
% Name: Yue Sun     ID: 2880445      Email:sunyue@u.northwestern.edu
% MP1: Flow Computation Lucas-Kanade's method and the Horn-Schunck's method


%% Setting the path
% Dear TA, please change this path corresponding to your computer! Thank you!
addpath('C:\Users\sunyue\Desktop\Computer Vision\Assignments\library')  % path of library
addpath('C:\Users\sunyue\Desktop\Computer Vision\Assignments\HW1\flow\office') % path of office
addpath('C:\Users\sunyue\Desktop\Computer Vision\Assignments\HW1\flow\rubic') % path of rubic
addpath('C:\Users\sunyue\Desktop\Computer Vision\Assignments\HW1\flow\sphere') % path of sphere


%% Library Description
% function [Vx, Vy] = LucasKanade(image1, image2, WindowSize, weighttype, sigma)
% Take two frame of images as Input as well as the window size and weight type
% and potential Gaussian sigma return Vx and Vy
% function [Ix, Iy, It] = IxIyIt(image1, image2)
% Compute the partial derivative Ix Iy of the image1 and It from image 1 to image 2
% function [Weight] = WeightMatrix(halfWindow, weighttype)
% Take half window size and weight type as Input and Weight Matrix as Output
% function [qx, qy, vx, vy] = QuiverVector(Vx, Vy, interval, image2)
% Take Vx Vy interval and opject image and return vector for quiver plot


%% Read the image file
% Read the image of Office
office0_rgb = imread('office.0.bmp');     office1_rgb = imread('office.1.bmp');      office2_rgb = imread('office.2.bmp');
office3_rgb = imread('office.3.bmp');     office4_rgb = imread('office.4.bmp');      office5_rgb = imread('office.5.bmp');
office6_rgb = imread('office.6.bmp');     office7_rgb = imread('office.7.bmp');      office8_rgb = imread('office.8.bmp');
office9_rgb = imread('office.9.bmp');     office10_rgb = imread('office.10.bmp');    office11_rgb = imread('office.11.bmp');
office12_rgb = imread('office.12.bmp');   office13_rgb = imread('office.13.bmp');    office14_rgb = imread('office.14.bmp');
office15_rgb = imread('office.15.bmp');   office16_rgb = imread('office.16.bmp');    office17_rgb = imread('office.17.bmp');
office18_rgb = imread('office.18.bmp');   office19_rgb = imread('office.19.bmp');
% Put all images in a cell
office_rgb = {office0_rgb, office1_rgb, office2_rgb, office3_rgb, office4_rgb,...
    office5_rgb, office6_rgb, office7_rgb, office8_rgb, office9_rgb,...
    office10_rgb, office11_rgb, office12_rgb, office13_rgb, office14_rgb,...
    office15_rgb, office16_rgb, office17_rgb, office18_rgb, office19_rgb};

% Read the image of Sphere
sphere0_rgb = imread('sphere.0.bmp');     sphere1_rgb = imread('sphere.1.bmp');      sphere2_rgb = imread('sphere.2.bmp');
sphere3_rgb = imread('sphere.3.bmp');     sphere4_rgb = imread('sphere.4.bmp');      sphere5_rgb = imread('sphere.5.bmp');
sphere6_rgb = imread('sphere.6.bmp');     sphere7_rgb = imread('sphere.7.bmp');      sphere8_rgb = imread('sphere.8.bmp');
sphere9_rgb = imread('sphere.9.bmp');     sphere10_rgb = imread('sphere.10.bmp');    sphere11_rgb = imread('sphere.11.bmp');
sphere12_rgb = imread('sphere.12.bmp');   sphere13_rgb = imread('sphere.13.bmp');    sphere14_rgb = imread('sphere.14.bmp');
sphere15_rgb = imread('sphere.15.bmp');   sphere16_rgb = imread('sphere.16.bmp');    sphere17_rgb = imread('sphere.17.bmp');
sphere18_rgb = imread('sphere.18.bmp');   sphere19_rgb = imread('sphere.19.bmp');
% put all images in a cell
sphere_rgb = {sphere0_rgb, sphere1_rgb, sphere2_rgb, sphere3_rgb, sphere4_rgb,...
    sphere5_rgb, sphere6_rgb, sphere7_rgb, sphere8_rgb, sphere9_rgb,...
    sphere10_rgb, sphere11_rgb, sphere12_rgb, sphere13_rgb, sphere14_rgb,...
    sphere15_rgb, sphere16_rgb, sphere17_rgb, sphere18_rgb, sphere19_rgb};

% Read the image of Rubic
rubic0_gray = imread('rubic.0.bmp');      rubic1_gray = imread('rubic.1.bmp');      rubic2_gray = imread('rubic.-1.bmp');
rubic3_gray = imread('rubic.2.bmp');      rubic4_gray = imread('rubic.3.bmp');      rubic5_gray = imread('rubic.4.bmp');
rubic6_gray = imread('rubic.5.bmp');      rubic7_gray = imread('rubic.6.bmp');      rubic8_gray = imread('rubic.7.bmp');     
rubic9_gray = imread('rubic.8.bmp');      rubic10_gray = imread('rubic.9.bmp');     rubic11_gray = imread('rubic.10.bmp');    
rubic12_gray = imread('rubic.11.bmp');    rubic13_gray = imread('rubic.12.bmp');    rubic14_gray = imread('rubic.13.bmp');    
rubic15_gray = imread('rubic.14.bmp');    rubic16_gray = imread('rubic.15.bmp');    rubic17_gray = imread('rubic.16.bmp');    
rubic18_gray = imread('rubic.17.bmp');    rubic19_gray = imread('rubic.18.bmp');    rubic20_gray = imread('rubic.19.bmp');
% put all images in a cell
rubic_gray = {rubic0_gray, rubic1_gray, rubic2_gray, rubic3_gray, rubic4_gray,...
    rubic5_gray, rubic6_gray, rubic7_gray, rubic8_gray, rubic9_gray,...
    rubic10_gray, rubic11_gray, rubic12_gray, rubic13_gray, rubic14_gray,...
    rubic15_gray, rubic16_gray, rubic17_gray, rubic18_gray, rubic19_gray};


%% Transfer the RGB image to Gray image
% Turn the office image from RGB to Gray
office0_gray = rgb2gray(office0_rgb);      office1_gray = rgb2gray(office1_rgb);      office2_gray = rgb2gray(office2_rgb);
office3_gray = rgb2gray(office3_rgb);      office4_gray = rgb2gray(office4_rgb);      office5_gray = rgb2gray(office5_rgb);
office6_gray = rgb2gray(office6_rgb);      office7_gray = rgb2gray(office7_rgb);      office8_gray = rgb2gray(office8_rgb);
office9_gray = rgb2gray(office9_rgb);      office10_gray = rgb2gray(office10_rgb);    office11_gray = rgb2gray(office11_rgb);
office12_gray = rgb2gray(office12_rgb);    office13_gray = rgb2gray(office13_rgb);    office14_gray = rgb2gray(office14_rgb);
office15_gray = rgb2gray(office15_rgb);    office16_gray = rgb2gray(office16_rgb);    office17_gray = rgb2gray(office17_rgb);
office18_gray = rgb2gray(office18_rgb);    office19_gray = rgb2gray(office19_rgb);
% Put all images in a cell
office_gray = {office0_gray, office1_gray, office2_gray, office3_gray, office4_gray,...
    office5_gray, office6_gray, office7_gray, office8_gray, office9_gray,...
    office10_gray, office11_gray, office12_gray, office13_gray, office14_gray,...
    office15_gray, office16_gray, office17_gray, office18_gray, office19_gray};

% Turn the office image from RGB to Gray
sphere0_gray = rgb2gray(sphere0_rgb);      sphere1_gray = rgb2gray(sphere1_rgb);      sphere2_gray = rgb2gray(sphere2_rgb);
sphere3_gray = rgb2gray(sphere3_rgb);      sphere4_gray = rgb2gray(sphere4_rgb);      sphere5_gray = rgb2gray(sphere5_rgb);
sphere6_gray = rgb2gray(sphere6_rgb);      sphere7_gray = rgb2gray(sphere7_rgb);      sphere8_gray = rgb2gray(sphere8_rgb);
sphere9_gray = rgb2gray(sphere9_rgb);      sphere10_gray = rgb2gray(sphere10_rgb);    sphere11_gray = rgb2gray(sphere11_rgb);
sphere12_gray = rgb2gray(sphere12_rgb);    sphere13_gray = rgb2gray(sphere13_rgb);    sphere14_gray = rgb2gray(sphere14_rgb);
sphere15_gray = rgb2gray(sphere15_rgb);    sphere16_gray = rgb2gray(sphere16_rgb);    sphere17_gray = rgb2gray(sphere17_rgb);
sphere18_gray = rgb2gray(sphere18_rgb);    sphere19_gray = rgb2gray(sphere19_rgb);
% Put all images in a cell
sphere_gray = {sphere0_gray, sphere1_gray, sphere2_gray, sphere3_gray, sphere4_gray,...
    sphere5_gray, sphere6_gray, sphere7_gray, sphere8_gray, sphere9_gray,...
    sphere10_gray, sphere11_gray, sphere12_gray, sphere13_gray, sphere14_gray,...
    sphere15_gray, sphere16_gray, sphere17_gray, sphere18_gray, sphere19_gray};


%% Testing with images
for i=2:10;
    current = rubic_gray{i};
    next = rubic_gray{i+1};
    display = rubic_gray{i+1};
    [Vx, Vy] = LucasKanade(current,next, 15, 1, 10);
    [qx, qy, vx, vy] = QuiverVector(Vx, Vy, 20, rubic_gray{i+1});
    figure();
    imshow(display);
    hold on;
    % draw the velocity vectors
    quiver(qx, qy, vx, vy, 'r','Linewidth',1.3)
end
