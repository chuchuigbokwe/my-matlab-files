function x0 = get_point_by_two_lines(l1,l2)
x0 = cross(l1,l2)

x0 = [x0(1)/x0(3); x0(2)/x0(3);1]