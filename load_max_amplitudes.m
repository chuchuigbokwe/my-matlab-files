%%Get Amplitudes for all data sets
function[max_amplitudes] = load_max_amplitudes()
data1 = load('insulcirc_ar1_sz3_dist0.15.mat');
data2 = load('insulcirc_ar2_sz3_dist0.15.mat');
data2_5 = load('insulcirc_ar2.5_sz3_dist0.15.mat');
data3 = load('insulcirc_ar3_sz3_dist0.15.mat');
data4 = load('insulcirc_ar4_sz3_dist0.15.mat');

% amp1_0  = data1.vols(1,:);
% amp1_90 = data1.vols(3,:);
% 
% y1_0 = max(amp1_0);
% index = find(amp1_0== max(amp1_0(:)))
% amplitude1 = [y1_0,index];
% 
% [rows,columns] = size()
amplitudes = {data1.vols,data2.vols,data2_5.vols,data3.vols,data4.vols};
max_amplitudes = [];
for i=1:length(amplitudes);
    [maxvals, maxindices] = sort(amplitudes{i}, 2, 'descend');
    maxvals = maxvals(:, 1:1);
    maxindices = maxindices(:, 1:1)
    C = [maxindices,maxvals];
    max_amplitudes = vertcat(max_amplitudes,C);
end