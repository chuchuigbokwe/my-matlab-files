% the partial derivatives Ix and Iy of the first image and It from image 1 to image 2
function [Ix, Iy, It] = ImageDerivatives(image1, image2)
Ix = conv2(double(image1), double(0.25 *[-1 1; -1 1]), 'same'); % partial devitive w.r.t x
Iy = conv2(double(image1), double(0.25 *[-1 -1; 1 1]), 'same'); % partial devitive w.r.t. y
It = conv2(double(image1), 1.0*ones(2), 'same') + conv2(double(image2), -1.0*ones(2), 'same'); % partial devitive w.r.t t
end