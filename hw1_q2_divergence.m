function two_d_hw_grad_wrapper()
% two_d_hw_grad_wrapper.m is a toy wrapper to illustrate the path
% taken by gradient descent depending on the learning rate (alpha) chosen.
% Here alpha is kept fixed and chosen by the use. The corresponding
% gradient steps, evaluated at the objective, are then plotted.  The plotted points on
% the objective turn from green to red as the algorithm converges (or
% reaches a maximum iteration count, preset to 50).
% (nonconvex) function here is 
%
% f(x) = -cos(2* pi * x.'* x)

% dials for the toy
alpha = 10^-2;     % step length/learning rate (for gradient descent). Preset to alpha = 10^-3
%7^-2 seems to diverge
%7^-3 seems to converge
x0 = [0;0.7];         % initial point (for gradient descent)

%%% perform gradient descent %%%
[x0,in,out] = gradient_descent(alpha,x0);

%%% plot function with grad descent objective evaluations %%%
plot_it_all(in,out)

%%% YOUR CODE GOES HERE %%%
    function [x0,in,out] = gradient_descent(alpha, x0)
        
  % initializations
    grad_stop = 10^-3;
    max_its = 50;
    iter = 1;
    grad = 1;
    in = [x0];
    out = [-cos(2*pi * transpose(x0) * x0)];% always treat transpose(x)*x as x^2 when differentiating
    % main loop
    while norm(grad) > grad_stop && iter <= max_its %for higher dimensions always use the norm of the gradient as opposed to its abs value
        % take gradient step
        grad = 4 * x0 *pi * sin(2 * pi * (transpose(x0) * x0));
        x0 = x0 - alpha*grad;

        % update containers
        in = [in x0];
        out = [out -cos(2*pi * transpose(x0) * x0)];

        % update stopers
        iter = iter + 1;
    end
end

% plots everything
function plot_it_all(in,out)
    % print function
    [A,b] = make_fun();
    
    % print steps on surface
    subplot(1,2,1)
    plot_steps(in,out,3)
    subplot(1,2,2)
    plot_steps(in,out,2)
    set(gcf,'color','w');
end

% plots everything
function [A,b] = make_fun()
    range = 1.3;                     % range over which to view surfaces
    [a1,a2] = meshgrid(-range:0.05:range);
    a1 = reshape(a1,numel(a1),1);
    a2 = reshape(a2,numel(a2),1);
    A = [a1, a2];
    A = (A.*A)*ones(2,1);
    b = -cos(2*pi*A);
    r = sqrt(size(b,1));
    a1 = reshape(a1,r,r);
    a2 = reshape(a2,r,r);
    b = reshape(b,r,r);
    subplot(1,2,1)
    surf(a1,a2,b)
    xlabel('x_1','Fontsize',18,'FontName','cmmi9')
    ylabel('x_2','Fontsize',18,'FontName','cmmi9')
    zlabel('f','Fontsize',18,'FontName','cmmi9')
    set(get(gca,'ZLabel'),'Rotation',0)
    set(gca,'FontSize',12);
    box on

    subplot(1,2,2)
    contourf(a1,a2,b,5)
    xlabel('x_1','Fontsize',14)
    ylabel('x_2 ','Fontsize',14)
    box on
    colormap gray
end

% plot descent steps on function surface
function plot_steps(in,out,dim)
    s = (1/length(out):1/length(out):1)';
    colorspec = [s.^(1),flipud(s) ,zeros(length(out),1)];
    width = (1 + s)*5;
    if dim == 2
        for i = 1:length(out)
            hold on
            plot(in(1,i),in(2,i),'o','Color',colorspec(i,:),'MarkerFaceColor',colorspec(i,:),'MarkerSize',width(i));
        end
    else % dim == 3
        for i = 1:length(out)
            hold on
            plot3(in(1,i),in(2,i),out(i),'o','Color',colorspec(i,:),'MarkerFaceColor',colorspec(i,:),'MarkerSize',width(i));
        end
    end
end

end
