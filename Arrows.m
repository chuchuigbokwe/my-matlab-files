
function [cx, cy, vx, vy] = Arrows(u, v, interval, image2)
vx = u(1:interval:end, 1:interval:end);
vy = v(1:interval:end, 1:interval:end);
% get coordinates
[m, n] = size(image2);
[x,y] = meshgrid(1:n, 1:m);
cx = x(1:interval:end, 1:interval:end);
cy = y(1:interval:end, 1:interval:end);
end