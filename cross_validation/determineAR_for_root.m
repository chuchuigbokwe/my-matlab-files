function determineAR_for_root()
clear all; close all;
testdata = load_data();

figure(1)
plot(testdata(:,1),testdata(:,2),'.')
axis([0 5 0.3 1]);
title('Training Data');

error = linear_fitting(testdata(:,1),testdata(:,2))
error_sqrt = sqrt_fitting(testdata(:,1),testdata(:,2))
%root_fitting();
end 

%%%--------------Local Functions-------------------------------------------

function testdata = load_data()
    data = load('/home/chu-chu/Documents/MATLAB/gauss_noise/ar1_ar2_ar3_ar4.mat'); 

    % Organize data
    mV_AMP = data.AMPMAT*1000 %use mV for amplitude as opposed to V
    AR = data.AR;
    testdata = [];
    for i=1:size(mV_AMP,2)
        dataARi = [AR(i)*ones(size(mV_AMP,1),1),mV_AMP(:,i)];
        testdata = [testdata; dataARi];
    end
end

%linear fitting: equation of y = x1*a + x0
function error = linear_fitting(a,b)

D = [ones(size(a)), a];
    
%{
%check to verify that cost function will be concave
Hessian = 2*(D')*D;
eig(Hessian);        %Eigenvalues are nonnegative
%}

%Least square optimization problem
xstar = pinv(D)*b;

%plot new dataline for model
dataline = (0:0.01:5);
Dnew = [ones(size(dataline))', dataline'];
b_new = Dnew*xstar;

figure(2)
plot(dataline, b_new);
hold on;
plot(a,b,'.');
title('Original space - First order fitting');
xlabel('Aspect Ratio');
ylabel('Amplitude (mV)');

%error squared (original model's error): 
error = (D*xstar-b).^2;
error = sum((D*xstar-b).^2);
end 

%square root fitting: equation of b = x1*sqrt(a) + x0
function error = sqrt_fitting(a,b)

D = [ones(size(a)), sqrt(a)]
    
%{
%check to verify that cost function will be concave
Hessian = 2*(D')*D;
eig(Hessian);        %Eigenvalues are nonnegative
%}

%Least square optimization problem
xstar = pinv(D'*D)*D'*b;

%plot new dataline for model in the ORIGINAL space
dataline = (0:0.01:5);
Dnew = [ones(size(dataline))', (dataline').^(1/2)];
b_new = Dnew*xstar;

figure(3)
plot(dataline, b_new);
hold on;
plot(a,b,'.');
title('Original Space - Square root fitting');
xlabel('Aspect Ratio');
ylabel('Amplitude (mV)');

%error squared (original model's error): 
error = (D*xstar-b).^2;
error = sum((D*xstar-b).^2);
end 