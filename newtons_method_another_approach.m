x= ones(10,1);%[1 ;1]%1.5;                                         %set starting value 
nmax=50;                                       %set max number of iterations 
eps=1;                                         %initialize error bound eps 
xvals=x;                                       %initialize array of iterates 
n=0; 
grad_stop = 10^-3;
%initialize n (counts iterations)
%f(x) = log(1 + exp(transpose(x) * x))
grad = (2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1);
while norm(grad)> grad_stop && n<=nmax                        %set while-conditions eps>=1e-5 &n<=nmax
    grad = (2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1);
    hess = (2*exp(transpose(x)*x)*(exp(transpose(x)*x) + 2*transpose(x)*x + 1))/(exp(transpose(x)*x) + 1)^2;
    y=x-grad/hess; %compute next iterate 
    xvals=[xvals;y];                           %write next iterate in array 
    eps=abs(y-x);                              %compute error 
    x=y;n=n+1;                                 %update x and n 
end                                            %end of while-loop
xvals