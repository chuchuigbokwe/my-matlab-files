%
function[Cx,Cy] = ManipulateImage(im1,im2,windowSize,sc)
% Reduce the size of the image
%sc = 2;
im2c = imresize(im2, 1/sc); 
C1 = corner(im2c);
C1 = C1*sc;

% Discard coners near the margin of the image
k = 1;
for i = 1:size(C1,1)
    x_i = C1(i, 2);
    y_i = C1(i, 1);
    if x_i-windowSize>=1 && y_i-windowSize>=1 && x_i+windowSize<=size(im1,1)-1 && y_i+windowSize<=size(im1,2)-1
      C(k,:) = C1(i,:);
      k = k+1;
    end
end
Cx = C(:,1);
Cy = C(:,2);



