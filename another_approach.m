x = ones(10,1);
hess = (2*exp(transpose(x)*x)*(exp(transpose(x)*x) + 2*transpose(x)*x + 1))/(exp(transpose(x)*x) + 1)^2;
grad = (2*x*exp(transpose(x)*x))/(exp(transpose(x)*x) + 1);
for i=0:10
    y = x
    x = y - grad/hess;
    x
    if x <= 10^-3
        break
    end
end
