
function [Weight] = WeightMatrix(hw, hsize, sigma)

weightsize = 2*hw +1; % Size of averaging filter
if hsize == 0 % uniform window, d = pinv(A).b
    weight = (1/weightsize)^2 * (ones(weightsize)); % Normalize the Matrix
elseif hsize == 1 % weighted window, d = ((A.'.W.A)^-1).A.'.W.b;
    weight = fspecial('gaussian',weightsize, sigma); % Gaussian Filter
end

weight = weight'; 
weight = weight(:); 
Weight = diag(weight); 
end
