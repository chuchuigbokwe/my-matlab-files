x = linspace(0,2*pi,50);
subplot(2,2,1), plot(x,sin(x),'k-.'),grid on, xlabel('x'),ylabel('sin(x)');
subplot(2,2,2), plot(x,cos(x),'r--'), xlabel('x'),ylabel('cos(x)');
subplot(2,2,3), plot(x,sin(2*x),'y:'), xlabel('x'),ylabel('sin(2x)');
subplot(2,2,4), plot(x,cos(2*x)),gxlabel('x'),ylabel('cos(2x)');