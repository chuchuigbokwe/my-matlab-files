load('hw_2_sin_data.mat');
% creates grid on the range [-3,3] x [-3,3] over which to plot surface
% x0 = linspace(-3,3,75);
% x1 = linspace(-3,3,75);
[x0,x1] = meshgrid(-3:0.1:3);
% create surface
an = data(:,1);
bn = data(:,2);
f = zeros(size(x0));
for i = 1:length(x0)
    for j = 1:length(x1)
        for k = 1:length(a)
            f(i,j) = (x0(i,i)*sin(2*pi.*a(k)*x1(j,j))-b(k)).^2;
%    f(i) = (x0(i).*sin(2*pi.*z*x1(i))-z).^2;
        end
    end
end
% plot surface
surf(x0,x1,f)


